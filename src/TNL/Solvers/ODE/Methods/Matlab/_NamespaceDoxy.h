// SPDX-FileComment: This file is part of TNL - Template Numerical Library (https://tnl-project.org/)
// SPDX-License-Identifier: MIT

#pragma once

/**
 * \brief Namespace for Matlab aliases for ODE solvers.
 */
namespace TNL::Solvers::ODE::Methods::Matlab {}
